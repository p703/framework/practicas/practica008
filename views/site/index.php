<?php

/* @var $this yii\web\View */
use yii\helpers\Html;



$this->title = 'Gestión ordenadores';
?>
<div class="site-index">

    <div class="body-content row">
        <div class='col-lg-4'>
            <?= Html::img('@web/imgs/ordenador.jpg',[
                'class'=>'img-fluid circle',
                'style'=>'-webkit-box-shadow: 2px 2px 50px 10px #FFB5FF; box-shadow: 2px 2px 50px 10px #FFB5FF; -webkit-border-radius: 50px 0; border-radius: 50px 0;'
                //'style'=>'border-radius:40px'
                
            ]) ?>
        </div>
        
            <div class="col-lg-7 text-justify">
                
                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                
            </div>
            
            
        </div>

    </div>
</div>
