<?php

use yii\grid\GridView;
use yii\helpers\Html;
?>


<div class="row">
<?= GridView::widget([
    'dataProvider'=>$dataProvider,
    'columns'=>[
                    'id',
                    'descripcion',
                    'procesador',
                    'memoria',
                    'discoduro',
                    [
                        'attribute'=>'ethernet',
                        'format'=>'raw',
                        'value'=>function($model){
                            //Colocar un checkbox deshabilitado
                            //return Html::activeCheckbox($model,'ethernet',['disabled'=>true]);
                            if($model->wifi){
                                return '<i class="fas fa-check-square"></i>';
                            }else{
                                return '<i class="fas fa-window-close"></i>';
                            }
                        }
                    ],
                    [
                        'attribute'=>'wifi',
                        'format'=>'raw',
                        'value'=>function($model){
                        //Colocar un checkbox deshabilitado
                        //return Html::activeCheckbox($model,'wifi',['disabled'=>true]);
                            if($model->wifi){
                                return '<i class="fas fa-check-square"></i>';
                            }else{
                                return '<i class="fas fa-window-close"></i>';
                            }
                        }
                    ],
                    'video',
                    [
                            'header' => 'Acciones',
                            'headerOptions' => ["class" => "btn-link font-weight-bold"],
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{insertar} {ver} {actualizar} {eliminar}',
                            'buttons' =>[
                                'insertar' => function ($url,$model,$key) {
                                    return Html::a('<i class="fas fa-plus-circle"></i>',
                                            [
                                                "site/insertar"
                                            ]);
                                },
                                'ver' => function ($url,$model,$key) {
                                    return Html::a('<i class="fas fa-eye"></i>',
                                            [
                                                "site/ver",
                                                "id"=>$model->id
                                            ]);
                                },
                                'actualizar' => function ($url,$model,$key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>',
                                            [
                                                "site/actualizar",
                                                "id"=>$model->id
                                            ]);
                                },
                                'eliminar' => function ($url,$model,$key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>',
                                            [
                                                "site/eliminar",
                                                "id"=>$model->id
                                            ],
                                            [
                                            'data'=>[
                                                'confirm'=>'¿Seguro que quieres eliminar?',
                                                'method'=>'post',
                                            ]
                                            ]
                                            );
                                },

                            ]
                    ]
                ]
        ]);
?>
</div>

<div class="row">
<?= Html::a('Crear ordenador',
        ['site/insertar'],
        ['class'=>'btn btn-primary']);
?>
</div>

