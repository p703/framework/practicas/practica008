﻿DROP DATABASE IF EXISTS ejemplo8;
CREATE DATABASE IF NOT EXISTS ejemplo8;
USE ejemplo8;

CREATE TABLE ordenadores(
  id int AUTO_INCREMENT,
  descripcion varchar(800),
  procesador varchar (255),
  memoria varchar(255),
  discoduro varchar(255),
  ethernet boolean,
  wifi boolean,
  video varchar(255),
  PRIMARY KEY(id)
  );