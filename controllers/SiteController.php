<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Ordenadores;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * MOSTRAR PAGINA DE INICIO
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAdministrar(){
        $dataProvider=new ActiveDataProvider([
            'query'=>Ordenadores::find()
        ]);
        
        return $this->render("administrar",[
            'dataProvider'=>$dataProvider,
        ]);
    }
    
    public function actionInsertar(){
       $model = new Ordenadores();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                // form inputs are valid, do something here
                return $this->redirect(["site/administrar"]);
            }
        }

        return $this->render('formulario', [
            'model' => $model,
            'boton'=>'Añadir',
            'titulo'=>'Añadir Ordenador'
        ]);
    }
    
    public function actionActualizar($id) {
        $model = Ordenadores::findOne($id);
    
        if($this->request->isPost){
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    // form inputs are valid, do something here
                    return $this->redirect(["site/administrar"]);
                }
    }
    }

    return $this->render('formulario', [
        'model' => $model,
        'boton'=>'Actualizar',
        'titulo'=>'Actualizar datos del ordenador'
    ]);
    }
    
    public function actionEliminar($id) {
        $model= Ordenadores::findOne($id);
        
        if($this->request->isPost){
            if ($model->load(Yii::$app->request->post())) {
                if($model->delete());
                    return $this->redirect(["site/administrar"]);
                }
        }
        return $this->render('formulario',[
           'model'=>$model,
           'boton'=>"Eliminar",
           'titulo'=>'<i class="far fa-trash-alt"></i> Eliminar ordenador'
        ]);
    }
    
    public function actionVer($id) {
        return $this->render("ver",[
            'model'=>Ordenadores::findOne($id),
        ]);
    }
    
    public function actionListar() {
        $dataProvider=new ActiveDataProvider([
            'query'=>Ordenadores::find()
        ]);
        
        return $this->render("listar",[
            'dataProvider'=>$dataProvider,
        ]);
    }
    
}